Introduction
=============

Some particulars about the install:

* Ubuntu 14.04
* Python virtual environments via Virtualenvwrapper
* Django
* nginx + uwsgi


Installation Steps
==================
I assume you are logged in as user `ubuntu`.

Update the OS. This could take some time:

    sudo apt-get update

Install required packages:

    sudo apt-get -y install git
    sudo apt-get -y install python-pip
    sudo apt-get -y install python-setuptools python-dev build-essential

We need supervisord for controlling some daemon processes, like uwsgi:

    sudo apt-get -y install supervisor

We'll install virtualenvwrapper for virtualenvs:

    sudo pip install virtualenvwrapper

Now edit `.bashrc`, adding these lines at the end:

    export WORKON_HOME=$HOME/.virtualenvs
    export PROJECT_HOME=$HOME/
    source /usr/local/bin/virtualenvwrapper.sh

Activate these changes on the command line:

	 source .bashrc

Now create a virtual environment:

    mkvirtualenv venv

This will activate the virtual environment.

Checkout code from Bitbucket:

    git clone git@bitbucket.org:premanandad/djangoapp.git

This requires your public key is on the server, alternatively you can checkout from https URL.

     git clone https://premanandad@bitbucket.org/premanandad/djangoapp.git

Now go into the working directory of the repo:

    cd djangoapp/
    pip install -r requirements.txt

Add uwsgi to supervisord:

    sudo ln -s /home/ubuntu/djangoapp/system/supervisord/uwsgi.conf /etc/supervisor/conf.d/uwsgi.conf

Now reload supervisor:

    sudo supervisorctl reload

This should start uwsgi right away. You can restart it this way in future:

    sudo supervisorctl restart uwsgi

Install nginx:

    sudo apt-get -y install nginx
	
Now symlink the configuration file:

    sudo ln -s /home/ubuntu/djangoapp/system/nginx/web.conf /etc/nginx/sites-enabled/


Now start nginx:

    sudo service nginx start

Check that it's running:

    sudo service nginx status

Check that the configuration is valid:

    sudo nginx -t



